const _ = require('lodash')
const checkTypes = require('check-types')
const methodsBySourceType = require('./methodsBySourceType.js')
const methods = require('./methods.js')
const Promise = require('bluebird')

module.exports = function(){
    this.fetchSources = function(args){
        if(!_.isObject(args)){
            throw new Error("Some arguments are expected.");
        }
        if(!_.isObject(args.options)){
            args.options = {}
        }
        checkTypes.assert.string(args.sourceType);
        if(typeof methodsBySourceType[args.sourceType] === 'undefined'){
            throw new Error("Unsopported source type.");
        }
        checkTypes.assert.array(args.sources);
        return new Promise(function (resolve, reject) {
            var completeSources = {},
            sourcesReference = {},
            rejected = false

            _.forEach(args.sources, function(v, k){
                sourcesReference[v] = true;
                methods[methodsBySourceType[args.sourceType]](v, args.options)
                .catch(function(error){
                    if(typeof args.onFail === 'function'){
                        args.onFail({
                            sources: sourcesReference,
                            source: v,
                            loadedSources: completeSources,
                            pendingSources: _.omit(sourcesReference, _.keys(completeSources)) 
                        })
                    }
                    rejected = true;
                    return reject(new Error('Error while loading source ' + v));
                    //return;
                })
                .then(function(){
                    if(args.stopOnReject === true && rejected === true){
                        return false;
                    }
                    completeSources[v] = true;
                    if(typeof args.onProgress === 'function'){
                        args.onProgress({
                            sources: sourcesReference,
                            source: v,
                            loadedSources: completeSources,
                            pendingSources: _.omit(sourcesReference, _.keys(completeSources)) 
                        });
                    }
                    if(_.size(completeSources) === _.size(sourcesReference)){
                        resolve(completeSources)
                    }
                })
            })
        });
    }
}