const _ = require('lodash')
const Promise = require('bluebird')
const checkTypes = require('check-types')
const imagePromise = require('image-promise')
const FontFaceObserver = require('fontfaceobserver')

module.exports = {
    fetchImages: function(source){
        checkTypes.assert.string(source);
        return imagePromise(source);
    },
    fetchFonts: function(source, options){
        if(!_.isObject(options)){
            options = {}
        }
        checkTypes.assert.string(source);

        var font = new FontFaceObserver(source);
        if(checkTypes.not.string(options.testString)){
            options.testString = 'A string test';
        }

        if(checkTypes.not.number(options.timeout)){
            options.timeout = 720000;//12 minutes
        }

        return font.load(options.testString, options.timeout);
    }
}